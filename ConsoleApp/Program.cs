﻿
using romanсalculator;
using RomanСalculator.Parsers;

Console.WriteLine(
            "Standard range of Roman numerals 1 - 3999 natural numbers.\n" +
            "Allowed characters \"IXCMVLD 0-9 * / + - ()\".\n" +
            "The numbers V, L, D cannot be repeated. The numbers I, X, C, M can be repeated no more than three times in a row.\n" +
            "I - 1, V - 5, X — 10, L — 50, C — 100, D — 500, M — 1000.\n" +
            "Calculator respects arithmetic rules.\n"+
            "The calculator also works with Arabic numerals 0-9.\n");

while (true)
{
    var input = Console.ReadLine();
    try
    {
        Console.WriteLine(ExpressionEvaluator.Calculate(input!));
    }
    catch (Exception ex)    
    {
        Console.WriteLine(ex.Message);
    }
}