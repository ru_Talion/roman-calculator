﻿
using Pidgin;
using RomanСalculator.Parsers;

namespace RomanСalculator
{
    internal abstract class Node
    {
        internal abstract decimal GetValue();
    }
    internal class RomanNumberNode : Node
    {
        private readonly string _digits;

        internal RomanNumberNode(string digits)
        {
            _digits = digits;
        }
        internal override decimal GetValue()
        {
            if (decimal.TryParse(_digits, out var result))
            {
                return result;
            }
            return RomanToNumberParser._numbers.ParseOrThrow(_digits);
        }
    }
    internal class BinaryOperationNode : Node
    {
        private readonly Node _left;
        private readonly Node _right;
        private readonly Func<decimal, decimal, decimal> _binaryOperation;
        internal BinaryOperationNode(
            Node left,
            Node right,
            Func<decimal, decimal, decimal> binaryOperation)
        {
            _left = left;
            _right = right;
            _binaryOperation = binaryOperation;
        }
        internal override decimal GetValue()
        {
            var leftValue = _left.GetValue();
            var rightValue = _right.GetValue();
            return _binaryOperation(leftValue, rightValue);
        }
    }
    internal class GroupNode : Node
    {
        private readonly Node _node;
        internal GroupNode(Node child)
        {
            _node = child;
        }
        internal override decimal GetValue()
        {
            return _node.GetValue();
        }
    }
}
