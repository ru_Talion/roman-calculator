﻿
using RomanСalculator.Parsers;
using RomanСalculator;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace romanсalculator
{
    public class ExpressionEvaluator
    {
        /// <summary>
        /// Performs mathematical operations on Roman or Arabic numerals.
        /// </summary>
        /// <exception cref="ValidationException"></exception>
        public static string Calculate(string inputValue)
        {
            var inputValueWithoutSpaces = inputValue.Replace(" ", "");
            var inputValidator = new InputParameterValidator();
            var outputValidator = new ParameterValidator();

            var validationResult = inputValidator.Validate(inputValueWithoutSpaces);

            if (validationResult.Errors.Any())
            {
                throw new ValidationException(validationResult.ToString());
            }

            try
            {
                var result1 = CalculatorParser.Parse(inputValueWithoutSpaces);
                //For large calculations, the XX,0000 format is returned and no parsing occurs.
                var result = Regex.Replace(result1, @"[,][0]*", string.Empty);
                var validationOutputResult = outputValidator.Validate(result);

                if (validationOutputResult.Errors.Any())
                {
                    throw new ValidationException(validationOutputResult.ToString());
                }
                return $"{NumberToRomanParser.IntegerToRoman(int.Parse(result))} ({result})";
            }
            catch (DivideByZeroException) { throw new ValidationException("Division by zero is not allowed."); } //test 19
            catch (ValidationException ex) { throw ex; }
        }




    }

}

