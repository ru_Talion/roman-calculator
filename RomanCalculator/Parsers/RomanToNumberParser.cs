﻿using Pidgin;
using System.ComponentModel.DataAnnotations;
using static Pidgin.Parser;

namespace RomanСalculator.Parsers
{
    internal static class RomanToNumberParser
    {
        internal static readonly Parser<char, int> _thousand = Token('M').ThenReturn(1000);
        internal static readonly Parser<char, int> _fiveHundred = Token('D').ThenReturn(500);
        internal static readonly Parser<char, int> _hundred = Token('C').ThenReturn(100);
        internal static readonly Parser<char, int> _fifty = Token('L').ThenReturn(50);
        internal static readonly Parser<char, int> _ten = Token('X').ThenReturn(10);
        internal static readonly Parser<char, int> _five = Token('V').ThenReturn(5);
        internal static readonly Parser<char, int> _one = Token('I').ThenReturn(1);

        internal static readonly Parser<char, int> _digits =
            OneOf(_thousand, _fiveHundred, _hundred, _fifty, _ten, _five, _one);

        internal static readonly Parser<char, int> _numbers =
            from res in _digits.AtLeastOnce()
            select Parse(res.ToArray());

        private static int Parse(int[] input)
        {
            int sum = 0;
            for (int i = 0; i < input.Length; i++)
            {
                var nextValue = i + 1 > input.Length - 1 ? 0 : input[i + 1];
                var nextТextValue = i + 2 > input.Length - 1 ? 0 : input[i + 2];
                var nextNextТextValue = i + 3 > input.Length - 1 ? 0 : input[i + 3];

                if (input[i] == nextValue & input[i] == nextТextValue & input[i] <= nextNextТextValue)
                {
                    throw new ValidationException("Only 2 characters can be subtracted (IIV)."); // test 18
                }
                if (input[i] < nextValue || input[i] < nextТextValue)
                {
                    sum -= input[i];
                }
                else
                {
                    sum += input[i];
                }
            }
            return sum;
        }
        internal static Parser<char, T> Token<T>(Parser<char, T> p) => Try(p).Before(SkipWhitespaces);
        internal static Parser<char, char> Token(char value) => Token(Char(value));
    }
}
