﻿using FluentValidation;
using System.Text;

namespace RomanСalculator.Parsers
{
    internal static class NumberToRomanParser
    {
        internal static string IntegerToRoman(int num)
        {
            var outputValidator = new ParameterValidator();
            var validationOutputResult = outputValidator.Validate(num.ToString());

            if (validationOutputResult.Errors.Any())
            {
                throw new ValidationException(validationOutputResult.ToString());
            }

            int[] values = { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
            string[] romanLiterals = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
            StringBuilder roman = new();

            for (int i = 0; i < values.Length; i++)
            {
                while (num >= values[i])
                {
                    num -= values[i];
                    roman.Append(romanLiterals[i]);
                }
            }
            return roman.ToString();
        }
    }
}
