﻿using Pidgin;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using static Pidgin.Parser;

[assembly: InternalsVisibleTo("RomanCalculator.Tests")]
namespace RomanСalculator.Parsers
{
    public static class CalculatorParser
    {
        private static readonly Parser<char, char> _thousand = Token('M');
        private static readonly Parser<char, char> _fiveHundred = Token('D');
        private static readonly Parser<char, char> _hundred = Token('C');
        private static readonly Parser<char, char> _fifty = Token('L');
        private static readonly Parser<char, char> _ten = Token('X');
        private static readonly Parser<char, char> _five = Token('V').Or(Token('5'));
        private static readonly Parser<char, char> _one = Token('I').Or(Token('1'));
        private static readonly Parser<char, char> _two = Token('2');
        private static readonly Parser<char, char> _three = Token('3');
        private static readonly Parser<char, char> _four = Token('4');
        private static readonly Parser<char, char> _six = Token('6');
        private static readonly Parser<char, char> _seven = Token('7');
        private static readonly Parser<char, char> _eight = Token('8');
        private static readonly Parser<char, char> _nine = Token('9');
        private static readonly Parser<char, char> _zero = Token('0');
        private static readonly Parser<char, char> _digits =
            OneOf(_thousand, _fiveHundred, _hundred, _fifty,
                _ten, _five, _one, _two, _three, _four, _six,
                _seven, _eight, _nine, _zero);

        private static readonly Parser<char, char[]> _numbers = _digits.AtLeastOnce().Map(x => x.ToArray());

        private static readonly Parser<char, char> _plus = Token('+');
        private static readonly Parser<char, char> _minus = Token('-');
        private static readonly Parser<char, char> _divide = Token('/');
        private static readonly Parser<char, char> _multiply = Token('*');

        private static readonly Parser<char, Func<decimal, decimal, decimal>> _plusFunc = _plus.ThenReturn((decimal x, decimal y) => x + y);
        private static readonly Parser<char, Func<decimal, decimal, decimal>> _minusFunc = _minus.ThenReturn((decimal x, decimal y) => x - y);
        private static readonly Parser<char, Func<decimal, decimal, decimal>> _divideFunc = _divide.ThenReturn((decimal x, decimal y) => x / y);
        private static readonly Parser<char, Func<decimal, decimal, decimal>> _multiplyFunc = _multiply.ThenReturn((decimal x, decimal y) => x * y);

        private static readonly Parser<char, Func<decimal, decimal, decimal>> _operatorsDivideAndMultiply = OneOf(_multiplyFunc, _divideFunc);
        private static readonly Parser<char, Func<decimal, decimal, decimal>> _operatorsPlusAndMinus = OneOf(_minusFunc, _plusFunc);

        private static readonly Parser<char, Node> _romanNumberNoda = _numbers.Map(x => new RomanNumberNode(new string(x)) as Node);

        private static readonly Parser<char, Node> _brackets =
           from result in Rec(() => _calculator!).Between(Token('('), Token(')'))
           select (new GroupNode(result) as Node);

        private static readonly Parser<char, Node> _numberNodaOrBracketNode = OneOf(Try(_brackets), Try(_romanNumberNoda));

        private static readonly Parser<char, (Node node, Func<decimal, decimal, decimal> operations)> _tail =
            from oper in _operatorsDivideAndMultiply
            from right in _numberNodaOrBracketNode
            select (right, oper);

        private static readonly Parser<char, Node> _binaryOperationsDivideAndMultiply =
            from left in _numberNodaOrBracketNode
            from tails in _tail.Many()
            select tails.Any() ? FromArrayToBranch(tails.ToArray(), left) : left;

        private static readonly Parser<char, (Node node, Func<decimal, decimal, decimal> operations)> _tailBinary =
           from oper in _operatorsPlusAndMinus
           from right in _binaryOperationsDivideAndMultiply
           select (right, oper);

        private static readonly Parser<char, Node> _calculator =
            from left in _binaryOperationsDivideAndMultiply
            from tails in _tailBinary.Many()
            select tails.Any() ? FromArrayToBranch(tails.ToArray(), left) : left;

        private static Node FromArrayToBranch((Node, Func<decimal, decimal, decimal>)[] tails, Node node)
        {
            var result = new BinaryOperationNode(node, tails.First().Item1, tails.First().Item2);

            for (int i = 1; i < tails.Length; i++)
            {
                result = new BinaryOperationNode(result, tails[i].Item1, tails[i].Item2);
            }
            return result;
        }
        private static Parser<char, T> Token<T>(Parser<char, T> p) => Try(p).Before(SkipWhitespaces);
        private static Parser<char, char> Token(char value) => Token(Char(value));

        internal static string Parse(string inputValue)
        {
            return _calculator.ParseOrThrow(inputValue).GetValue().ToString();
        }
    }
}
