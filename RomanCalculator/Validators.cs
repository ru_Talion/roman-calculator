﻿using FluentValidation;
using System.Text;
using System.Text.RegularExpressions;

namespace RomanСalculator
{
    public class InputParameterValidator : AbstractValidator<string>
    {
        private static readonly string[] _notAllowed = new string[] { "VV", "LL", "DD", "IIII", "XXXX", "MMMM", "CCCC" };
        private static readonly string[] _notAllowedBracket = new string[] { "()", ")(" };
        private const string _consecutiveOperatorsRegex = @"[\*\-\+/]{2,}";
        private const string _operatorInBrackets = @"[(][/*\-+][)]";
        private const string _operatorAtStart = @"^[/*\-+]";
        private const string _operatorAtEnd = @"[/*\-+]+$";
        private const string _letterWithNumber = "[IXCMVLD][0-9]";
        private const string _numberWithletter = "[0-9][IXCMVLD]";
        private const string _bracketNumbers = "[)][IXCMVLD0-9]";
        private const string _numbersBracket = "[IXCMVLD0-9][(]";
        private const string _bracketOperator = @"[(][/*\-+]";
        private const string _operatorBracket = @"[/*\-+][)]";
        public InputParameterValidator()
        {
            RuleFor(input => input)
                .NotNull()
                .NotEmpty().WithMessage(@"Empty input is not allowed.")
                .Matches(@"^[IXCMVLD0-9\*/\+\-\(\) ]+$").WithMessage(@"Allowed characters ""IXCMVLD 0-9 * / + - ()""") // test 1
                .Matches(@"[IXCMVLD0-9]").WithMessage("Calculations must contain values IXCMVLD 0-9 ") // test 3
                .Must(x => !Regex.IsMatch(x, _consecutiveOperatorsRegex)).WithMessage("Consecutive operators are not allowed") // test 4
                .Must(x => !Regex.IsMatch(x, _operatorInBrackets)).WithMessage("Operator cannot be in brackets.") // test 5
                .Must(x => !Regex.IsMatch(x, _operatorAtStart)).WithMessage("Operator at the beginning is not allowed") //test 6
                .Must(x => !Regex.IsMatch(x, _operatorAtEnd)).WithMessage("Operator cannot be at the end.") //test 7
                .Must(x => !Regex.IsMatch(x, _letterWithNumber)).WithMessage("A mixture of letters and numbers is not allowed.") //test 8
                .Must(x => !Regex.IsMatch(x, _numberWithletter)).WithMessage("A mixture of numbers and letters is not allowed.") //test 9
                .Must(x => !Regex.IsMatch(x, _bracketNumbers)).WithMessage("Missing operator between bracket and digit.") //test 10
                .Must(x => !Regex.IsMatch(x, _numbersBracket)).WithMessage("Missing operator between digit and bracket.") //test 11
                .Must(x => !Regex.IsMatch(x, _bracketOperator)).WithMessage(@"Missing digit after bracket ""("".") //test 12
                .Must(x => !Regex.IsMatch(x, _operatorBracket)).WithMessage(@"Missing digit before bracket "")"".") //test 13
                .Must(x => !_notAllowed.Any(x.Contains)).WithMessage("The numbers V, L, D cannot be repeated. The numbers I X C M cannot be repeated more than 3 times.") // test 2
                .Must(x => !_notAllowedBracket.Any(x.Contains)).WithMessage(@"Combinations of empty open "")("" or closed ""()"" brackets are not allowed.") //test 14
                .Must(CheckingParentheses).WithMessage(@"The number of open and closed brackets must be equal. Starting with "")"" and ending with ""("" is not allowed. Empty brackets ""()"" are not allowed."); //test 15
        }
        private bool CheckingParentheses(string input)
        {
            // We remove everything. We leave the brackets.
            var output = Regex.Replace(input, @"[^()]", string.Empty);
            var sb = new StringBuilder(output);
            var length = sb.Length;

            for (int i = 0; i <= length / 2; i++)
            {
                sb.Replace("()", "");
            }
            return sb.Length == 0;
        }
    }
    public class ParameterValidator : AbstractValidator<string>
    {
        public ParameterValidator()
        {
            RuleFor(input => input)
                 .NotNull()
                 .NotEmpty()
                 .Must(IsInteger)
                 .WithMessage($"The output parameter cannot be represented by a Roman numeral.") //test 16
                 .Must(x => decimal.Parse(x) >= 1 & decimal.Parse(x) <= 3999)
                 .WithMessage($"The input and output parameters must be in the range 1 - 3999."); //test 17
        }
        private bool IsInteger(string input)
        {
            return int.TryParse(input, out var _);
        }
    }
}
