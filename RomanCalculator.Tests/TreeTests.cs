﻿using FluentAssertions;
using RomanСalculator;

namespace RomanCalculator.Tests
{
    public class TreeTests
    {
        [Fact]
        public void Tree()
        {
            // 6 + (14 / (2 + 5)) = 8
            // VI + (XIV / ( II + V)) = IIX
            var plus = (decimal x, decimal y) => x + y;
            var division = (decimal x, decimal y) => x / y;
            BinaryOperationNode BinaryOperationNode =
            new(
                new RomanNumberNode("VI"),
                new GroupNode(
                    new BinaryOperationNode(
                        new RomanNumberNode("XIV"),
                        new GroupNode(
                            new BinaryOperationNode(
                                new RomanNumberNode("II"),
                                new RomanNumberNode("V"),
                                plus)),
                        division)),
                plus);

            var result = BinaryOperationNode.GetValue();
            result.Should().Be(8);
        }
    }
}
