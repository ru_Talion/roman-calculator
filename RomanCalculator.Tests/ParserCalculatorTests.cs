using FluentAssertions;
using Pidgin;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Text;
using Roman�alculator.Parsers;
using roman�alculator;

namespace RomanCalculator.Tests
{
    public class ParserCalculatorTests
    {
        [Theory]
        // Various calculation options are created in Excel.
        [InlineData("X", "X (10)")]
        [InlineData("10", "X (10)")]
        [InlineData("(10)", "X (10)")]
        [InlineData("V+5", "X (10)")]
        [InlineData("(V+5)", "X (10)")]
        [InlineData("(V+5)+1", "XI (11)")]
        [InlineData("1+(V+5)", "XI (11)")]
        [InlineData("3+III*III+3", "XV (15)")]
        [InlineData("3*III+III*3", "XVIII (18)")]
        [InlineData("(VIII+IV)*2-VI/3", "XXII (22)")]
        [InlineData("(9+3)*5/6-II", "VIII (8)")]
        [InlineData("8/4+(6-II)*3", "XIV (14)")]
        [InlineData("(4+II)*(7-5)+IIX", "XX (20)")]
        [InlineData("10-II*4+7/7", "III (3)")]
        [InlineData("2*(4+6)/V+7", "XI (11)")]
        [InlineData("(6+III)*(8-5)-4", "XXIII (23)")]
        [InlineData("V*3-6+9/3", "XII (12)")]
        [InlineData("7/(3-II)+IIX*2", "XXIII (23)")]
        [InlineData("(9+5)*II-7/VII", "XXVII (27)")]
        [InlineData("8-III*(5-II)+9", "VIII (8)")]
        [InlineData("3*(II+5)/7-1", "II (2)")]
        [InlineData("(7-4)*6/II+5", "XIV (14)")]
        [InlineData("2*4+6/(9-III)", "IX (9)")]
        [InlineData("3+2*(7-4)-6", "III (3)")]
        [InlineData("(6-II)*3+IIX/4", "XIV (14)")]
        [InlineData("8/II+7*(5-III)", "XVIII (18)")]
        [InlineData("V*(6-II)/II+1", "XI (11)")]
        [InlineData("4*(8-6)+9", "XVII (17)")]
        [InlineData("2*6+(7-III)", "XVI (16)")]
        [InlineData("IX-V+IIX/4", "VI (6)")]
        [InlineData("V*(II+1)-4/2", "XIII (13)")]
        [InlineData("(6+2)*4/8+3", "VII (7)")]
        [InlineData("2*3*IV+V*6-7", "XLVII (47)")]
        [InlineData("8-4/2*III+7", "IX (9)")]
        [InlineData("V+6*7-8/II", "XLIII (43)")]
        [InlineData("V+6/2*4-7", "X (10)")]
        [InlineData("4*(8-5)/II+7", "XIII (13)")]
        [InlineData("V+6*7/(II+1)", "XIX (19)")]
        [InlineData("(7-5)*(III+6)-8", "X (10)")]
        [InlineData("IX/3+2*(5-1)", "XI (11)")]
        [InlineData("(4+2)*(IX-7)-III", "IX (9)")]
        [InlineData("6*(4-II)+IIX/4", "XIV (14)")]
        [InlineData("V*(7-II)/V+3", "VIII (8)")]
        [InlineData("(6-III)*(8-6)+7", "XIII (13)")]
        [InlineData("IX-II*(8/2)+5", "VI (6)")]
        [InlineData("3+VII*(6-IV)-5", "XII (12)")]
        [InlineData("(V+3)*(9-6)+1", "XXV (25)")]
        [InlineData("2*VI-8/(5-III)", "VIII (8)")]
        [InlineData("6+3*8/(4-II)", "XVIII (18)")]
        [InlineData("(9-4)*III+7/7", "XVI (16)")]
        [InlineData("(4-III)*8/II+7", "XI (11)")]
        [InlineData("7/(6-V)+IIX*3", "XXXI (31)")]
        [InlineData("(7-II)*4/II+6", "XVI (16)")]
        [InlineData("8-4*(III-1)+7", "VII (7)")]
        [InlineData("2*(V+3)-6/3", "XIV (14)")]
        [InlineData("6-II*(6/2)+7", "VII (7)")]
        [InlineData("V*8/(6-II)+3", "XIII (13)")]
        [InlineData("6*(7-5)-9/3", "IX (9)")]
        [InlineData("2*(6-4)*7-8", "XX (20)")]
        [InlineData("4*(5-III)+IIX/4", "X (10)")]
        [InlineData("(7-II)*(4-I)-6", "IX (9)")]
        [InlineData("3+6*(9-8)+7", "XVI (16)")]
        [InlineData("(II+3)*4-6/3", "XVIII (18)")]
        [InlineData("8/(4-II)*6-5", "XIX (19)")]
        [InlineData("6+2*(9/3)+4", "XVI (16)")]
        [InlineData("4*(7-III)+6/2", "XIX (19)")]
        [InlineData("IX-III*(4-1)+6", "VI (6)")]
        [InlineData("2*6+IIX/(4-II)", "XVI (16)")]
        [InlineData("V*(7-II)/V+4", "IX (9)")]
        [InlineData("7+IIX*(6-III)-5", "XXVI (26)")]
        [InlineData("6/3*(8-5)+2", "VIII (8)")]
        [InlineData("(9-II)*4/II+1", "XV (15)")]
        [InlineData("IX*(II+1)+IIX/4-6+7", "XXX (30)")]
        [InlineData("(V+2)*9/3-8+(1+6)", "XX (20)")]
        [InlineData("4+5*8/2-(1+6)*3", "III (3)")]
        [InlineData("3*4/II+5*(6-1)-7", "XXIV (24)")]
        [InlineData("IX/3+7*(4-II)-8", "IX (9)")]
        [InlineData("(4-1)*9-8/II+7", "XXX (30)")]
        [InlineData("V-6+IIX*4/II+(1+3)", "XIX (19)")]
        [InlineData("2*(6+5)+IIX-4/2", "XXVIII (28)")]
        [InlineData("7-III+9/3*(8-II)", "XXII (22)")]
        [InlineData("6*(3+1)+5-8/2", "XXV (25)")]
        [InlineData("(9-III)*2/4+1+IIX", "XII (12)")]
        [InlineData("1+2*(5-III)+9-6/3", "XII (12)")]
        [InlineData("(8-III)*7/V+6-1", "XII (12)")]
        [InlineData("6/3-V+2*(9-1)", "XIII (13)")]
        [InlineData("IX-6/3+7*(2-1)", "XIV (14)")]
        [InlineData("8/4-V+6*(3-II)", "III (3)")]
        [InlineData("2*(4+3)-8/4+5", "XVII (17)")]
        [InlineData("2*6+IIX/4+(1+7)*3", "XXXVIII (38)")]
        [InlineData("(MMMDCCXXIV-MMCCXXIX)*II", "MMCMXC (2990)")]
        [InlineData("8/4*((9-(V))+2)", "XII (12)")]
        [InlineData("(1+8*(3-2)/4)+7", "X (10)")]
        [InlineData("((8-3)*6/(5+5))-1", "II (2)")]
        [InlineData("(((2+2/(2+2)*2)*(2+2/(2+2)*2))*((2+2/(2+2)*2)*(2+2/(2+2)*2)))", "LXXXI (81)")]
        public void CalculatorCalculations(string input, string expected)
        {
            ExpressionEvaluator.Calculate(input).Should().Be(expected);
        }

        [Fact]
        public void ThrowingAllTypesOfExceptions()
        {
            // Tested exceptions like "//test {number}"
            Action test1 = () => ExpressionEvaluator.Calculate("fssfas");
            test1.Should().Throw<ValidationException>().WithMessage("*Allowed characters \"IXCMVLD 0-9 * / + - ()*");

            Action test2 = () => ExpressionEvaluator.Calculate("XXXX - X / II");
            test2.Should().Throw<ValidationException>().WithMessage("*The numbers V, L, D cannot be repeated. The numbers I X C M cannot be repeated more than 3 times.*");

            Action test3 = () => ExpressionEvaluator.Calculate("-");
            test3.Should().Throw<ValidationException>().WithMessage("*Calculations must contain values IXCMVLD 0-9*");

            Action test4 = () => ExpressionEvaluator.Calculate("5 ++ V");
            test4.Should().Throw<ValidationException>().WithMessage("*Consecutive operators are not allowed*");

            Action test5 = () => ExpressionEvaluator.Calculate("V (+) 5");
            test5.Should().Throw<ValidationException>().WithMessage("*Operator cannot be in brackets*");

            Action test6 = () => ExpressionEvaluator.Calculate("+ 5 + V");
            test6.Should().Throw<ValidationException>().WithMessage("*Operator at the beginning is not allowed*");

            Action test7 = () => ExpressionEvaluator.Calculate("5 + V /");
            test7.Should().Throw<ValidationException>().WithMessage("*Operator cannot be at the end*");

            Action test8 = () => ExpressionEvaluator.Calculate("V5 + IV");
            test8.Should().Throw<ValidationException>().WithMessage("*A mixture of letters and numbers is not allowed*");

            Action test9 = () => ExpressionEvaluator.Calculate("5V / 5");
            test9.Should().Throw<ValidationException>().WithMessage("*A mixture of numbers and letters is not allowed*");

            Action test10 = () => ExpressionEvaluator.Calculate("(V+5)5");
            test10.Should().Throw<ValidationException>().WithMessage("*Missing operator between bracket and digit*");

            Action test11 = () => ExpressionEvaluator.Calculate("10(5+V)");
            test11.Should().Throw<ValidationException>().WithMessage("*Missing operator between digit and bracket*");

            Action test12 = () => ExpressionEvaluator.Calculate("10+(+V)");
            test12.Should().Throw<ValidationException>().WithMessage("*Missing digit after bracket*");

            Action test13 = () => ExpressionEvaluator.Calculate("10+(5+)");
            test13.Should().Throw<ValidationException>().WithMessage("*Missing digit before bracket*");

            Action test14 = () => ExpressionEvaluator.Calculate("10+(5+V)-()");
            test14.Should().Throw<ValidationException>().WithMessage("*Combinations of empty open \")(\" or closed \"()\" brackets are not allowed*");

            Action test15 = () => ExpressionEvaluator.Calculate("10+(5+VII))");
            test15.Should().Throw<ValidationException>().WithMessage("*The number of open and closed brackets must be equal. Starting with \")\" and ending with \"(\" is not allowed. Empty brackets \"()\" are not allowed*");

            Action test16 = () => ExpressionEvaluator.Calculate("V/III");
            test16.Should().Throw<ValidationException>().WithMessage("*The output parameter cannot be represented by a Roman numeral*");

            Action test17 = () => ExpressionEvaluator.Calculate("1500*VI");
            test17.Should().Throw<ValidationException>().WithMessage("*The input and output parameters must be in the range 1 - 3999*");

            Action test18 = () => ExpressionEvaluator.Calculate("III*IIIX");
            test18.Should().Throw<ValidationException>().WithMessage("*Only 2 characters can be subtracted (IIV)*");

            Action test19 = () => ExpressionEvaluator.Calculate("V/0");
            test19.Should().Throw<ValidationException>().WithMessage("*Division by zero is not allowed*");

            Action test20 = () => ExpressionEvaluator.Calculate("");
            test20.Should().Throw<ValidationException>().WithMessage("*Empty input is not allowed*");
        }

        [Fact]
        // 22*22*22 = 10648 combinations.
        // If you think about all the options of 3 characters, they cover 99.99% of the errors.
        public void CheckAllThePresenceOf3Characters()
        {
            char[] validCharacters = new char[]
            { 'X', 'M', 'I', 'V', 'L', 'C', 'D',
                '1', '2', '4', '5', '6', '7', '8', '9', '0',
                '+', '-', '*', '/', '(', ')'};

            StringBuilder input = new();

            for (int i = 0; i < validCharacters.Length; i++)
            {
                for (int j = 0; j < validCharacters.Length; j++)
                {
                    for (int u = 0; u < validCharacters.Length; u++)
                    {
                        input.Clear();
                        input.Append(validCharacters[i]);
                        input.Append(validCharacters[j]);
                        input.Append(validCharacters[u]);
                        Trace.WriteLine($"------->  {input}");
                        try
                        {
                            var test = ExpressionEvaluator.Calculate(input.ToString());
                            test.Should().BeOfType(typeof(string));
                            Trace.WriteLine($"------->>>>>>>>  {test}");
                        }
                        catch (ValidationException ex)
                        {
                            Assert.Throws<ValidationException>(() => ExpressionEvaluator.Calculate(input.ToString()));
                        }
                    }
                }
            }
        }
        [Fact]
        public void GenerationOfRandomInputs()
        {
            char[] validCharacters = new char[]
            { 'X', 'M', 'I', 'V', 'L', 'C', 'D',
                '1', '2', '4', '5', '6', '7', '8', '9', '0',
                '+', '-', '*', '/', '(', ')'};

            Random rnd = new Random();
            for (int i = 0; i < 10000; i++)
            {
                StringBuilder input = new();
                int rndlength = rnd.Next(1, 6);

                for (int j = 0; j < rndlength; j++)
                {
                    input.Append(validCharacters[rnd.Next(0, validCharacters.Length)]);
                }
                Trace.WriteLine($"------->  {input}");
                try
                {
                    var test = ExpressionEvaluator.Calculate(input.ToString());
                    test.Should().BeOfType(typeof(string));
                }
                catch (ValidationException ex)
                {
                    Trace.WriteLine($"------->>>>>>>>  {ex}");
                    Assert.Throws<ValidationException>(() => ExpressionEvaluator.Calculate(input.ToString()));
                }
            }
        }
    }
}